﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlockChain
{
    class Program
    {
        List<Block> blockChain = new List<Block>();

        static void Main(string[] args)
        {
            int previousHash = 0;
            string[] genesisTransctions = { "Satishi sent Arianna 10000 bitcoins", "Jeremy sent 10 bitcoins to Hugo" };
            Block genesisBlock = new Block(previousHash, genesisTransctions);
            
            string[] transactions = { "Rashid sent 100 bitcoins to Amazon", "Kerry sent 20 bitcoins to Dianna" };
            Block block1 = new Block(genesisBlock.GetBlockHash(), transactions);

            Console.WriteLine("Genesis Block:");
            Console.WriteLine(genesisBlock.GetBlockHash());
            Console.WriteLine("Block1:");
            Console.WriteLine(block1.GetBlockHash());
            Console.ReadLine();
        }
    }
}
