﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlockChain
{
    class Block
    {
        private int previousHash;
        private string[] transactions;

        private int blockHash;

        public Block(int previousHash, string[] transactions)
        {
            this.previousHash = previousHash;
            this.transactions = transactions;

            blockHash = CreateArrayHash(transactions, previousHash);
        }

        public int GetPreviousHash()
        {
            return previousHash;
        }

        public string[] GetTransactions()
        {
            return transactions;
        }

        public int GetBlockHash()
        {
            return blockHash;
        }

        private int CreateArrayHash(string[] trans, int hash) {
            var finalHash = 0;
            foreach (string t in trans)
            {
                finalHash += t.GetHashCode();                
            }
            finalHash += hash;

            return finalHash;
        }
    }
}
